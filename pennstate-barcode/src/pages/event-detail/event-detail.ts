import { Component, ViewChild } from '@angular/core';
import { ActionSheetController, AlertController, Content, ModalController, NavController, NavParams, Platform, ViewController, ToastController } from 'ionic-angular';
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { AddUser } from '../users/add-users/add-user';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { HelperService } from '../../providers/helperService';
import { Talisma } from '../../providers/crm/talisma';
import { AlertModal } from '../alert-modal/alert-modal';
import * as moment from 'moment';
import * as _ from 'underscore';
import { PostAddPage } from '../posts/post-add/post-add';

@Component({
  selector: 'page-event-detail',
  templateUrl: 'event-detail.html'
})

export class EventDetailPage {
  event: any = {};
  selectedModerators: any = [];
  moderatorsIds: any[] = [];
  addedModeratorIds: any[] = [];
  participants: any[] = [];
  isEdit: boolean = false;
  basicEventData = {};
  uid: any;
  participantSegment: string = 'attended';
  remainingParticipantsCount: number;
  participationStatusValue: number = 3;
  roleValue: any;
  currentTime: any;
  timeZone: string = 'America/Matamoros';
  //object consists of all status of the particiapant
  participationStatus: any = {
    1: "Invited",
    2: "Registered",
    3: "Attended",
    4: "Canceled",
    5: "CancellationRequested",
    6: "Interested",
    7: "InterestedInFutureEvents",
    8: "Pending",
    9: "Confirmed",
    10: "Approved",
    11: "Declined",
    12: "FollowUp",
    13: "NoShow",
    14: "Transferred",
    15: "Substituted",
    16: "Waitlisted",
    17: "PaymentFailed",
    18: "Deleted",
    19: "WaitlistRTR",
    20: "RegisteredPayLater",
    21: "TransferredPayLater",
    22: "SubstitutedPayLater",
    23: "EventCancelled"
  }
  @ViewChild(Content) content: Content;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public afoDatabase: AngularFireOfflineDatabase,
    public alertCtrl: AlertController,
    private barcodeScanner: BarcodeScanner,
    public helperService: HelperService,
    public modalCtrl: ModalController,
    private nav: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public talisma: Talisma,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController,
  ) {
    this.init();
  }

  init(ionRefresher?: any) {
    this.helperService.showLoading();
    let eventId = this.navParams.get('event') ? this.navParams.get('event').EventId : null;
    return new Promise((resolve) => {
      this.afoDatabase.object('/events/' + eventId)
        .subscribe((event: any) => {
          this.event = event;
          this.remainingParticipantsCount = this.event.ParticipationCount - this.event.RegisteredParticipants;
          resolve();
        })
    })
      .then(() => {
        return this.helperService.getUid()
      })
      .then((value: any) => {
        this.uid = value;
        return this.helperService.getRoleValue()
      })
      .then((value: any) => {
        this.roleValue = value;
        return new Promise((resolve) => {
          if (this.event && this.uid && this.event.EventId && this.roleValue == 99) {
            this.afoDatabase.list('/moderatorEvents/')
              .subscribe((snapshots: any) => {
                let temp: any = [];
                for (let snapshot of snapshots) {
                  let keys = Object.keys(snapshot);
                  if (snapshot && snapshot.$key) {
                    for (let key of keys) {
                      if (key == this.event.EventId && temp.indexOf(snapshot.$key) < 0) {
                        temp.push(snapshot.$key);
                      }
                    }
                  }
                }
                this.moderatorsIds = Object.assign([], temp);
                resolve();
              });
          }
          else {
            resolve();
          }
        });
      })
      .then(() => {
        this.basicEventData = { eventId: this.event.EventId, eventName: this.event.Name, startDate: this.event.EventStartDate, endDate: this.event.EventEndDate, city: this.event.EventCity, venue: this.event.EventVenue }
        if (this.roleValue !== 99) {
          this.getParticipants()
            .then(() => {
              if (ionRefresher) {
                ionRefresher.complete();
              }
              this.helperService.hideLoading();
            })
            .catch((err) => {
              this.helperService.hideLoading();
              console.log('err occured', err)
            });
        }
        else {
          this.helperService.hideLoading();
        }
      });
  }

  openMoreActionSheet() {
    let buttons: any = [
      {
        text: 'Post to Activity Stream',
        icon: !this.platform.is('ios') ? 'create' : null,
        handler: () => {
          let post: any = { type: 'CALENDAR', sessionId: this.event.EventId, caption: '' };
          this.nav.push(PostAddPage, { post: post, isCalendarType: true })
        }
      },
      {
        text: 'Cancel',
        icon: !this.platform.is('ios') ? 'close' : null,
        role: 'cancel'
      }
    ];
    let actionSheet = this.actionSheetCtrl.create({
      title: 'More options',
      buttons: buttons,
      cssClass: 'customButton',

    })
    actionSheet.present();
  }

  isEventLive() {
    return ((moment(this.event.EventStartDate).tz(this.timeZone).format('x')) >= (moment().tz(this.timeZone).subtract(1, 'days').endOf('day').format('x'))) ? true : false;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  toggleEdit() {
    if (this.isEdit) {
      this.selectedModerators = [];
    }
    this.isEdit = !this.isEdit;
  }

  // select moderator to delete
  selectModerator(userId: any) {
    if (!this.isEdit) {
      return false;
    }
    if (this.selectedModerators[userId]) {
      delete this.selectedModerators[userId];
    } else {
      this.selectedModerators[userId] = true;
    }
  }

  removeModerators() {
    let keys = Object.keys(this.selectedModerators), promiseArray: any[] = [], removedModerators: any[] = [], removedModeratorIds: any[] = [];
    let msg: any = keys.length == 1 ? 'this moderator' : 'these moderators';
    let alert = this.alertCtrl.create({
      title: 'Confirmation',
      message: 'Are you sure to permanently delete ' + msg + ' ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Continue',
          handler: () => {
            this.helperService.showLoading();
            let self = this;
            keys.forEach(function(key) {
              promiseArray.push(
                new Promise((resolve, reject) => {
                  delete self.selectedModerators[key];
                  removedModeratorIds.push(key);
                  self.afoDatabase.object('/moderatorEvents/' + key + '/' + self.event.EventId)
                    .remove()
                    .then(() => {
                      self.afoDatabase.object('/users/' + key)
                        .take(1).subscribe((userData: any) => {
                          removedModerators.push({ name: `${userData.firstName} ${userData.lastName}`, mail: userData.email });
                          resolve();
                        })
                    })
                    .catch((err) => {
                      console.log(err);
                      reject(err);
                    });
                })
              )
            });
            Promise.all(promiseArray)
              .then(() => {

                let message: any = { eventData: JSON.stringify({ eventId: this.event.EventId, eventName: this.event.Name }), title: 'Moderator access revoked', body: 'You are revoked from moderator role for the event: ' + this.event.Name, landingPage: JSON.stringify({ type: 'COMPONENT', value: 'NavigationDashboardPage' }), createdDate: moment.utc().format('x'), userIds: removedModeratorIds, target: 'Event Notification', editId: 'newnotification', createdBy: this.uid }
                try {
                  this.toggleEdit();
                  this.helperService.sendNotification(message);
                  this.helperService.sendEventEmail(removedModerators, { subject: 'You have been removed as moderator', body: message.body, event: this.basicEventData });
                  this.helperService.hideLoading();
                  this.helperService.showMessage('Moderators removed successfully');
                }
                catch (err) {
                  this.helperService.hideLoading();
                  this.helperService.showMessage('Unable to remove Moderators');
                  console.log(err);
                }
              })
              .catch(err => {
                this.helperService.hideLoading();
                this.helperService.showMessage('Unable to remove Moderators');
                console.log(err);
              })
          }
        }
      ]
    });
    alert.present();
  }

  saveModerators() {
    this.helperService.showLoading();
    let self = this;
    let temp: any = {
      EventId: this.event.EventId,
      Name: this.event.Name,
      EventStartDate: this.event.EventStartDate,
      Notes: this.event.Notes || '',
      EventVenue: this.event.EventVenue || '',
      ParticipationCount: this.event.ParticipationCount || '',
      RegisteredParticipants: this.event.RegisteredParticipants || ''
    }

    // add event data to each moderator node
    let addedModerators: any[] = [], promiseArray: any = [];
    this.moderatorsIds.forEach(function(userId) {
      promiseArray.push(
        new Promise((resolve, reject) => {
          self.afoDatabase.object('/moderatorEvents/' + userId + '/' + self.event.EventId)
            .update(temp)
            .then(() => {
              self.afoDatabase.object('/users/' + userId)
                .take(1).subscribe((userData: any) => {
                  addedModerators.push({ name: `${userData.firstName} ${userData.lastName}`, mail: userData.email });
                  resolve();
                })
            })
            .catch((err: any) => {
              console.log(err);
              reject(err);
            });
        })
      )
    });
    Promise.all(promiseArray)
      .then(() => {
        try {
          let message: any = { enableEventView: 'true', eventData: JSON.stringify({ eventId: this.event.EventId, eventName: this.event.Name }), title: 'Promoted as Moderator', body: 'You have been added as moderator to the event: ' + this.event.Name, landingPage: JSON.stringify({ type: 'SCHEDULE', value: this.event.EventId }), createdDate: moment.utc().format('x'), userIds: this.addedModeratorIds, target: 'Event Notification', editId: 'newnotification', createdBy: this.uid }
          this.helperService.sendNotification(message);
          this.helperService.sendEventEmail(addedModerators, { subject: 'You have been assigned as moderator', body: message.body, event: this.basicEventData });
          this.helperService.hideLoading();
          self.helperService.showMessage('Moderators added successfully');
        }
        catch (err) {
          this.helperService.hideLoading();
          self.helperService.showMessage('Failed to add moderator');
          this.addedModeratorIds = [];
          console.log(err);
        }
      })
      .catch((err) => {
        self.helperService.showMessage('Failed to add moderator');
        this.helperService.hideLoading();
        this.addedModeratorIds = [];
        console.log(err);
      })
  }

  canDelete() {
    return Object.keys(this.selectedModerators).length;
  }

  addModerators() {
    if (this.isEdit) {
      this.toggleEdit();
    }
    let modal = this.modalCtrl.create(AddUser, { users: this.moderatorsIds });
    modal.present();

    modal.onDidDismiss((data: any) => {
      if (data && data.length) {
        this.addedModeratorIds = _.difference(data, this.moderatorsIds);
        this.moderatorsIds = data;
        this.saveModerators();
      }
    });
  }

  getParticipants() {
    return new Promise((resolve, reject) => {
      this.afoDatabase.list('/participants/' + this.event.EventId)
        .subscribe((participants: any) => {
          this.participants = participants;
          resolve();
        })
    })
  }

  // getParticipants() {
  //   return new Promise((resolve, reject) => {
  //     this.talisma.participants.findBy({ key: 'EventNameId', value: this.event.EventId })
  //       .then((response: any) => {
  //         this.participants = response;
  //         resolve();
  //       })
  //       .catch((err) => {
  //         console.log('Err occured', err);
  //       })
  //   })
  // }

  isStatus(status: any) {
    if (status == 'Attended' && this.participantSegment == 'attended') {
      return true;
    } else if (status != 'Attended' && this.participantSegment == 'remaining') {
      return true;
    } else if (this.participantSegment == 'all') {
      return true;
    }
  }

  getColor(status: any) {
    if (status == 'Attended') {
      return 'secondary';
    } else if (status == 'Registered') {
      return 'primary';
    } else {
      return 'dark';
    }
  }

  getRemainingParticipant() {
    let all: any, attended: any;
    if (!this.event.ParticipationCount || !this.participants.length) {
      all = 0;
    }

    all = this.event.ParticipationCount >= this.participants.length ? this.event.ParticipationCount : this.participants.length;
    attended = this.event.AttendedParticipants ? this.event.AttendedParticipants : 0;

    return all - attended;
  }

  participantSegmentChanged(segment: any) {
    if (this.participantSegment == 'attended') {
      this.participationStatusValue = 3;
    }
    else if (this.participantSegment == 'remaining') {
      this.participationStatusValue = 2;
    }
    else {
      this.participationStatusValue = 3;
    }

    setTimeout(() => {
      this.content.scrollToTop();
    })
  }

  scan() {
    let self: any = this;
    return new Promise((resolve, reject) => {
      self.barcodeScanner.scan()
        .then((barcodeData: any) => {
          resolve((barcodeData.cancelled) ? false : barcodeData.text);
        }, (err: any) => {
          reject(err);
        });
    });
  }

  continousScan() {
    let self: any = this;

    self.scan()
      .then((participantId: any) => {
        if (!participantId) {
          this.init();
          return false;
        }
        this.markAttended(participantId, 'Scanner');
      });
  }

  manual() {
    this.alertCtrl.create({
      title: 'Check-in',
      inputs: [
        {
          name: 'participantId',
          placeholder: 'Confirmation ticket number',
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Continue',
          handler: (data: any) => {
            if (data.participantId) {
              this.markAttended(data.participantId, 'Manual');
            }
            else {
              this.showModalAlert('failure', 'Please enter a Valid Participant Id');
            }
          }
        }
      ]
    }).present();
  }

  showModalAlert(status: string, message?: string) {
    let self = this;
    return new Promise(function(resolve, reject) {
      let object: any = { status: status };
      message ? object['message'] = message : '';
      let alert = self.modalCtrl.create(AlertModal, object);
      alert.present();
      alert.onDidDismiss((data: any) => {
        resolve();
      })
    });
  }


  // get Attended participant count w.r.t event
  // get all attended participants from 'participants' node
  // return only count
  getAttendedParticipant() {
    return new Promise((resolve) => {
      this.afoDatabase.list('/participants/' + this.event.EventId, { query: { orderByChild: 'status', equalTo: 'Attended' } })
        .subscribe((attendedParticipants: any) => {
          resolve(attendedParticipants.length);
        })
    });
  }

  // set AttendedParticipants in 'event' node
  setAttendedParticipants(count: any) {
    return this.afoDatabase.object('/events/' + this.event.EventId).update({ AttendedParticipants: count });
  }

  // mark participant as attended
  // check if he is Registered w.r.t event
  // update in participant node
  // update AttendedParticipant count in events node
  markAttended(participantId: any, type?: any) {
    let participant = _.findWhere(this.participants, { 'uid': participantId })
    if (!participant) {
      this.showModalAlert('failure', 'Participant does not belong to this event')
      return false;
    }
    if (participant.status == 'Attended') {
      this.showModalAlert('failure', 'Participant already checked-in')
      return false;
    } else if (participant.status != 'Registered') {
      this.showModalAlert('failure', 'Participant is not registered')
      return false;
    }
    this.afoDatabase.object('/participants/' + this.event.EventId + '/' + participantId)
      .update({ status: 'Attended' })
      .then(() => {
        return this.afoDatabase.object('/events/' + this.event.EventId + '/AttendedParticipants')
          .set(++this.event.AttendedParticipants);
      })
      .then(() => {
        return this.getAttendedParticipant();
      })
      .then((attendedParticipantsCount: any) => {
        return this.setAttendedParticipants(attendedParticipantsCount);
      })
      .then(() => {
        return this.showModalAlert('success');
      })
      .then(() => {
        if (type == 'Scanner') {
          this.continousScan();
        }
      })
  }
  
  getParticipationColor(status: string) {
    let color: any
    switch (status) {
      case 'Registered': {
        color = 'primary'
        break;
      }
      case 'Attended': {
        color = 'secondary'
        break;
      }
      case 'Interested': {
        color = 'grey'
        break;
      }
      case 'Cancelled': {
        color = 'danger'
        break;
      }
    }
    return color;
  }
  // markAttended(participantId: any, type: string) {
  //   this.talisma.participants.markAttended(participantId, this.event.EventId)
  //     .then((response: string) => {
  //       let status: string = '';
  //       if (response.toLowerCase().match('successfully') === null) {
  //         status = 'failure';
  //         if (participantId == '0000') {
  //           status = 'success';
  //         }
  //       } else {
  //         status = 'success';
  //       }
  //       this.showModalAlert(status)
  //         .then(() => {
  //           if (type == 'Scanner') {
  //             this.continousScan();
  //           }
  //         })
  //     })
  // }
}
