import { Component } from '@angular/core';
import { ToastController, NavController } from 'ionic-angular';
import { AngularFire } from 'angularfire2';
import { HelperService } from '../../../providers/helperService';
import * as moment from 'moment';
import * as _ from 'underscore';

@Component({
  selector: 'page-send-invite',
  templateUrl: 'send-invite.html'
})

export class SendInvite {
  email: any;
  uid: any;
  loading: any;

  constructor(
    public af: AngularFire,
    public toastCtrl: ToastController,
    public helperService: HelperService,
    public navCtrl: NavController
  ) {
    this.helperService.getUid()
      .then((value: any) => {
        this.uid = value;
      });
  }

  upsertUser(user: any) {
    this.af.database.object('/users/' + user.uid)
      .set(user)
      .then(() => {
        this.helperService.hideLoading();
        this.email = '';
        this.helperService.showMessage('Invitation sent successfully', 2000);
        if (this.navCtrl.canGoBack()) {
          this.navCtrl.pop();
        }
      })
  }

  sendInvite() {
    this.helperService.showLoading();
    this.af.database.object('/users/' + this.uid)
      .subscribe((userData: any) => {
        if ((userData.email.toLowerCase()) != (this.email.toLowerCase())) {
          this.helperService.isUserExists(this.email)
            .then((user: any) => {
              let index = _.findIndex(user, { 'status': 'accepted' });
              if (index > -1) {
                this.helperService.showMessage('Already an member');
                this.helperService.hideLoading();
                return false;
              }
              this.sendMail(user.length ? user[0] : []);
            });
        }
        else {
          this.helperService.hideLoading();
          this.helperService.showMessage('Cannot Invite Current User');
        }
      });

  }

  sendMail(user: any) {
    this.email = user.email ? user.email : this.email;
    this.helperService.sendOtp(this.email, 'sendInvite')
      .then((res: any) => {

        if (res.msg != 'success') {
          this.helperService.hideLoading();
          return false;
        }

        let code = res.otp;

        if (user && user.isUserExists) {
          user.appName = 'ADMIN';
          user.inviteModifiedDate = moment.utc().format('x');
          user.inviteModifiedBy = this.uid;
          user.code = code;
          this.upsertUser(user);
        } else {
          let temp: any = {
            uid: moment.utc().format('x'),
            email: this.email.toLowerCase(),
            code: code,
            appName: 'ADMIN',
            status: 'pending',
            invitedBy: this.uid,
            invitedDate: moment.utc().format('x')
          }
          this.upsertUser(temp);
        }
      })
  }

}
