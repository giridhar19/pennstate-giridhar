import { Component } from '@angular/core';
import { ActionSheetController, AlertController, NavController, NavParams, LoadingController, Platform, ToastController } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { HelperService } from '../../providers/helperService';
import { CreatePage } from '../pages/page-add/page-add';
import * as _ from 'underscore';
import moment from 'moment';

@Component({
  selector: 'custom-page',
  templateUrl: 'custom-page.html'
})

export class CustomPage {
  page: any = {};
  pageId: string;
  showBackButton: boolean = false;

  constructor(
    public params: NavParams,
    public afoDatabase: AngularFireOfflineDatabase,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public helperService: HelperService
  ) {
    this.showBackButton = this.params.get("showBackButton");
    let loading = this.loadingCtrl.create();
    loading.present();
    this.pageId = params.get('pageId');
    afoDatabase.object('/pages/' + this.pageId)
      .subscribe((page) => {
        if (page.title) {
          this.page = page;
        }
        loading.dismiss();
      });
  }

  formatDate(date: any) {
    return moment.utc(date).format('lll');
  }

  // show admin edit option edit/delete
  showOptions() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Options',
      buttons: [{
        text: 'Edit Page',
        icon: !this.platform.is('ios') ? 'create' : null,
        handler: () => {
          this.navCtrl.push(CreatePage, { pageId: this.page.id });
        }
      },
      {
        text: 'Delete Page',
        icon: !this.platform.is('ios') ? 'trash' : null,
        role: 'destructive',
        handler: () => {
          let confirm = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Are you sure to delete this page?',
            buttons: [
              {
                text: 'Cancel'
              },
              {
                text: 'Continue',
                handler: () => {
                  let loading = this.loadingCtrl.create({
                    content: 'request in progress wait ...'
                  })
                  loading.present();
                  let pageId = this.page.id;
                  this.afoDatabase.object('/pages/' + this.page.id)
                    .remove()
                    .then(() => {
                      this.afoDatabase.object('/menus')
                        .take(1).subscribe((menus) => {
                          let tabIndex = _.findIndex(menus.TAB_MENU, { 'value': pageId });
                          if (tabIndex > -1) {
                            menus.TAB_MENU.splice(tabIndex, 1)
                          }
                          let menuIndex = _.findIndex(menus.SIDE_MENU, { 'value': pageId });
                          if (menuIndex > -1) {
                            menus.SIDE_MENU.splice(menuIndex, 1)
                          }
                          return this.afoDatabase.object('/menus')
                            .set(menus)
                            .then(() => {
                              loading.dismiss();
                              this.toastCtrl.create({
                                message: 'Page deleted successfully',
                                duration: 2000
                              }).present();
                              this.navCtrl.pop();
                            })
                        });
                    });
                }
              }
            ]
          });
          confirm.present();
        }
      },
      {
        text: 'Cancel',
        role: 'Cancel',
        icon: !this.platform.is('ios') ? 'close' : null
      }
      ]
    });
    actionSheet.present();
  }
}
