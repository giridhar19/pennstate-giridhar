import { Component } from '@angular/core';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { LoadingController, ToastController } from 'ionic-angular';

@Component({
  selector: 'menu',
  templateUrl: 'menu.html'
})

export class MenuPage {
  isListExpanded: boolean = false;
  modifiedMenus: any = [];
  isSaveSuccessful: boolean;
  isSaveFailed: boolean;

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) { }

  toggleList(type: any) {
    this.isListExpanded = !(this.isListExpanded);
  }

  saveAllMenus() {
    let self = this;
    let counter = this.modifiedMenus.length;
    let loading = this.loadingCtrl.create({
      content: 'Saving wait ...'
    })
    loading.present();

    for (let i = 0; i < this.modifiedMenus.length; i++) {
      this.saveMenu(this.modifiedMenus[i])
        .then(function() {
          counter--;
          if (!counter) {
            self.modifiedMenus = [];
            loading.dismiss();
            self.toastCtrl.create({
              message: 'Menus are saved successfully',
              duration: 2000
            }).present();
          }
        });
    }
  }

  saveMenu(menu: any) {
    return this.afoDatabase.object('/menus/' + menu.menuName).set(menu.menuItems);
  }

  updateMenu(data: any) {
    let index = this.modifiedMenus.map(function(e: any) { return e.menuName; }).indexOf(data.menuName);
    if (index == -1) {
      this.modifiedMenus.push({ menuName: data.menuName, menuItems: data.menuItems });
    }
  }
}
