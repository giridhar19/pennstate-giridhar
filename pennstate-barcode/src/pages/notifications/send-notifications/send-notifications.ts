import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoadingController, NavParams, ViewController, AlertController, ModalController } from 'ionic-angular';
import { AngularFire } from 'angularfire2';
import { HelperService } from '../../../providers/helperService';
import * as firebase from 'firebase';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { FirebaseAnalyticsProvider } from '../../../providers/firebase-analytics-provider';
import * as moment from 'moment';
import { AddMenu } from '../../../components/menuPages/addMenu/addMenu';
import { AudienceListing } from '../audience-listing/audience-listing';
import { UserData } from '../../../providers/user-data';

@Component({
  selector: 'page-send-notifications',
  templateUrl: 'send-notifications.html',
  providers: [HelperService, FirebaseAnalyticsProvider]
})

export class SendNotificationsPage {
  message?: any = {};
  targets: string[] = [];
  recipients: any[] = [];
  userLive: boolean;
  edit: boolean;
  selectedTargets: any[] = [];
  roleValue: any;
  uid:any;

  constructor(
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    private alertCtrl: AlertController,
    public navParams: NavParams,
    public firebaseAnalytics: FirebaseAnalyticsProvider,
    public helperService: HelperService,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public userData: UserData,
    public viewCtrl: ViewController
  ) {
    if (navParams.get('notification')) {
      this.message = Object.assign({}, navParams.get('notification'));
      this.edit = navParams.get('edit');
      if (this.message.landingPage) {
        this.message.landingPage = JSON.parse(this.message.landingPage);
      }
    }
    let self = this;
    this.userData.getRoleValue()
      .then((roleValue) => {
        this.roleValue = roleValue;
        if (self.roleValue != 99) {
          self.message.target == 'Selected Audience';
        }
      })
      this.userData.getUid()
        .then((uid) => {
          this.uid = uid;
        })
    let connectedRef = firebase.database().ref(".info/connected");
    connectedRef.on("value", function(snap) {
      self.userLive = snap.val();
    });
  }

  dismiss() {
    this.viewCtrl.dismiss('cancel');
  }

  isDisabled(form: NgForm) {
    if (this.message.target == 'Selected Audience') {
      return !(form.valid && this.message && this.message.landingPage && this.message.landingPage.value && this.selectedTargets.length > 0);
    }
    // return !(form.valid && this.message && this.message.landingPage && this.message.landingPage.value);
    return !(form.valid && this.message);
  }

  addTarget() {
    let modal = this.modalCtrl.create(AddMenu,
      {
        data: this.message.landingPage,
        options: 'notificationTargetTypes',
        title: 'Define Action',
        target: 'Notification Target',
        isMenu: false,
        isNotification: true
      })
    modal.present();

    modal.onDidDismiss((data: any) => {
      if (data) {
        this.message.landingPage = data;
      }
    })
  }

  sendNotification() {
    if (this.userLive) {
      // Send new notification or clone
      if (!this.edit) {
        this.message.editId = 'newnotification';
        this.message.createdDate = moment.utc().format('x');
      }
      // edit existing notification
      else {
        this.message.editId = this.message.id || this.message.editId
        this.message.createdDate = this.message.createdDate;
      }
      let loading = this.loadingCtrl.create({
        content: 'Sending Notification, Please wait...'
      })
      loading.present();
      this.message.topics = this.selectedTargets;
      this.message.createdBy = this.uid;
      // this.message.landingPage = JSON.stringify(this.message.landingPage);

      
      this.helperService.sendNotification(this.message)
        .then((res: any) => {
          if (this.helperService.isCordovaEnabled()) {
            this.firebaseAnalytics.logEvent('SEND_NOTIFICATION',
              {
                status: 'success',
                recipient: this.message.target
              })
          }
          loading.dismiss();
          this.viewCtrl.dismiss();
        })
        .catch((err: any) => {
          if (this.helperService.isCordovaEnabled()) {
            this.firebaseAnalytics.logEvent('SEND_NOTIFICATION',
              {
                status: 'failure',
                recipient: this.message.target,
                reason: err
              })
          }
        })
    } else {
      let alert = this.alertCtrl.create({
        title: 'Network is offline',
        subTitle: 'Check your wireless/Carrier network settings',
        buttons: ['Dismiss']
      });
      alert.present();
    }

  }

  selectAudience() {
    let modal = this.modalCtrl.create(AudienceListing)
    modal.present();

    modal.onDidDismiss((data: any) => {
      if (data) {
        this.selectedTargets = data;
      }
    })
  }
}
