import { Component } from '@angular/core';
import { AngularFireOfflineDatabase } from 'angularfire2-offline';
import { AlertController, LoadingController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { HelperService } from '../../../providers/helperService';

@Component({
  selector: 'page-user-management',
  templateUrl: 'user-management.html',
  providers: [HelperService]
})

export class UserMangementPage {

  userUid: string;
  user?: any = {};
  action: string = 'flushUserData';

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public alertCtrl: AlertController,
    public helperService: HelperService,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public viewCtrl: ViewController
  ) {
    this.userUid = this.navParams.get('userUid');
    if (this.userUid) {
      this.afoDatabase.object('/users/' + this.userUid)
        .take(1)
        .subscribe((user) => {
          this.user = user ? user : {};
        })
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }


  manageUser(user: any) {
    let loading = this.loadingCtrl.create(),
      msg: string;
    loading.present();
    return new Promise((resolve, reject) => {
      if (this.action == 'flushUserData') {
        this.helperService.flushUserData(this.user.id)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          })
      }
      else {
        this.helperService.flushUser(this.user.id)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          })
      }
    })
      .then(() => {
        loading.dismiss();
        this.toastMsg(this.action == 'flushUserData' ? 'User data has been flushed sucessfully' : 'User data has been deleted sucessfully');
      })
      .catch((err) => {
        loading.dismiss();
        this.toastMsg(this.action == 'flushUserData' ? 'Failed to flush the user data' : 'Failed to delete the user');
        console.log('err occured', err)
      })
  }

  toastMsg(msg: string) {
    this.toastCtrl.create({
      message: msg,
      duration: 1500
    }).present();
  }

  popupAlert() {
    let alert = this.alertCtrl.create({
      title: 'Are you sure?',
      message: 'You want to block/ unblock user',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Continue',
          handler: () => {
            // this.manageUser(this.user);
          }
        }
      ]
    });
    alert.present();
  }
}
