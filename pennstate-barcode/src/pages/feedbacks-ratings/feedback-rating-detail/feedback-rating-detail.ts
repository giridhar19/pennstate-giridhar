import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { AngularFire } from 'angularfire2';
import * as _ from 'underscore';

@Component({
  selector: 'page-feedback-rating-detail',
  templateUrl: 'feedback-rating-detail.html'
})
export class FeedbackRatingDetailPage {
  group: any = {};
  maxRating: any = [0, 1, 2, 3, 4];
  feedbacks: any[] = [];
  ratingOne: number = 0;
  ratingTwo: number = 0;
  ratingThree: number = 0;
  ratingFour: number = 0;
  ratingFive: number = 0;
  public doughnutChartLabels: string[] = ['1 Star', '2 Stars', '3 Stars', '4 Stars', '5 Stars'];
  public doughnutChartData: number[] = [0, 0, 0, 0, 0];

  constructor(
    public af: AngularFire,
    public navParams: NavParams
  ) {
    this.group = _.values(this.navParams.get('feedback'));
    if (this.group) {
      let self = this;
      _.each(this.group, function(val, key) {
        self.feedbacks.push(val);
        switch (self.group[key].rating) {
          case 1: {
            self.ratingOne += 1;
            break;
          }
          case 2: {
            self.ratingTwo += 1;
            break;
          }
          case 3: {
            self.ratingThree += 1;
            break;
          }
          case 4: {
            self.ratingFour += 1;
            break;
          }
          case 5: {
            self.ratingFive += 1;
            break;
          }
        }
      })
      this.doughnutChartData = [this.ratingOne, this.ratingTwo, this.ratingThree, this.ratingFour, this.ratingFive];
    }
  }
}
