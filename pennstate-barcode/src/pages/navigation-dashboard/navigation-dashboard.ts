import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { FirebaseAnalyticsProvider } from '../../providers/firebase-analytics-provider';
import { StatusBar } from '@ionic-native/status-bar';
import { HelperService } from '../../providers/helperService';
import { Talisma } from '../../providers/crm/talisma';
import * as _ from 'underscore';
import { ModeratorList } from '../moderator-list/moderator-list';
import { EventListPage } from '../event-list/event-list';

@Component({
  selector: 'page-navigation-dashboard',
  templateUrl: 'navigation-dashboard.html',
  providers: [FirebaseAnalyticsProvider]
})

export class NavigationDashboardPage {
  slides: any[];
  menuItems: any[] = [];
  title: string;
  events: any[] = [];
  eventListPage: any = {
    icon: 'calendar',
    title: 'Events',
    type: 'COMPONENT',
    value: 'EventListPage'
  };
  featuredContentPerPage: number;
  isProcessing: boolean = true;
  roleValue: any;
  uid: string;

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public firebaseAnalytics: FirebaseAnalyticsProvider,
    private navCtrl: NavController,
    public navParams: NavParams,
    private statusBar: StatusBar,
    public userdata: UserData,
    public helper: HelperService,
    public talisma: Talisma
  ) {
    this.statusBar.backgroundColorByHexString('#488aff');
    this.isProcessing = true;
    this.title = navParams.get('title') ? navParams.get('title') : 'Home';
    firebaseAnalytics.setScreen('NAVIGATION_DASHBOARD');

    // homesliders url from db
    this.afoDatabase.list('/settings/homePage', { preserveSnapshot: true })
      .subscribe((snapshots: any) => {
        this.slides = snapshots && snapshots.length ? snapshots : [];
      });

    //fetches the menus in homepage
    this.afoDatabase.list('/menus/MODERATOR_HOME_PAGE')
      .subscribe((menuItems) => {
        this.menuItems = menuItems ? menuItems : [];

      });
    let self: any = this;
    let subUrl = '?$select=Name,EventStartDate&$filter=(EventStartDate ge ' + this.helper.getTimeStamp() + ') &$orderby=EventStartDate asc';
    this.userdata.getRoleValue()
      .then((roleValue) => {
        self.roleValue = roleValue;
        if (self.roleValue == 99) {
          self.talisma.events.getEvents(5, 0, subUrl)
            .then((response: any) => {
              self.events = response;
              self.isProcessing = false;
            })
            .catch((err: any) => {
              console.log('err occured', err);
            });
        }
        else {
          self.userdata.getUid()
            .then((uid: any) => {
              self.uid = uid;
              self.afoDatabase.list('/moderatorEvents/' + self.uid, { query: { orderByChild: 'EventStartDate' } })
                .subscribe((allEvents: any) => {
                  self.events = [];
                  let temp = _.filter(allEvents, function(event: any) {
                    if (event.EventStartDate >= self.helper.getTimeStamp()) {
                      return event.EventId;
                    }
                  });
                  self.events = temp.slice(0, 5);
                  self.isProcessing = false;
                });
            })
        }
      })
  }

  getTriplets() {
    let triplets: any[] = [];
    let length = this.menuItems.length;
    for (let i = 0; i < length; i += 3) {
      let triples: any[] = [];
      triples.push(this.menuItems[i]);
      if (i + 1 < length) {
        triples.push(this.menuItems[i + 1]);
      }
      if (i + 2 < length) {
        triples.push(this.menuItems[i + 2]);
      }
      triplets.push(triples);
    }
    return triplets;
  }

  openEventListPage() {
    this.navCtrl.push((this.roleValue == 20 ? ModeratorList : EventListPage), { hideBackButton: false, uid: this.uid });
  }

  ngOnDestroy() {
    this.statusBar.backgroundColorByHexString('#488aff');
  }
}
