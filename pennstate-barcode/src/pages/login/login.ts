import { Component, ViewChild } from '@angular/core';
import { AngularFireOfflineDatabase } from 'angularfire2-offline';
import { AngularFire } from 'angularfire2';
import { Events, MenuController, NavController, NavParams, Platform, Slides, ToastController } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';

import { UserData } from '../../providers/user-data';
import { HelperService } from '../../providers/helperService';
import { VerifyInvite } from '../invites/verify-invite/verify-invite';
import { NavigationDashboardPage } from '../navigation-dashboard/navigation-dashboard';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {

  signupSlide: any = {};
  loginSlide: any = {};
  forgotPasswordSlide: any = {};
  resetPasswordSlide: any = {};
  title: string = "";
  generatedOtp: any = '';
  otp: any = '';
  previousSlide: string;
  user: any = {};
  hideBackButton: boolean = true;
  showFooter: boolean = true;
  pushPage: any = VerifyInvite;
  email: string;
  bannerPicture: string;
  @ViewChild(Slides) slides: Slides;

  constructor(
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public helperService: HelperService,
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public toastCtrl: ToastController,
    public userData: UserData,
    private keyboard: Keyboard,
    public events: Events
  ) {
    this.title = 'Login';
    this.menuCtrl.swipeEnable(false);
    this.menuCtrl.enable(false);
    // homesliders url from db
    this.afoDatabase.object('/settings/homePage', { preserveSnapshot: true })
      .subscribe((snapshot: any) => {
        this.bannerPicture = snapshot ? snapshot[0] : '';
      });
    // hide footer on keyboard show
    this.keyboard.onKeyboardShow()
      .subscribe(() => {
        this.showFooter = false;
      });

    // show footer on keyboard hide
    this.keyboard.onKeyboardHide()
      .subscribe(() => {
        this.showFooter = true;
      });

    //get fcm token
    if (!this.platform.is('core')) {
      //sets the FCM token to DB once the user loggedIn
      this.af.auth.subscribe((auth) => {
        if (auth) {
          let uid = auth.uid;
          this.userData.getFCMToken()
            .then((token) => {
              if (token) {
                this.af.database.object('/notificationTokens/' + uid)
                  .set({ 'token': token, type: ((this.platform.is('android') && !this.platform.is('core')) ? 'android' : 'ios') });
              }
            })
        }
      })
    }

    setTimeout(() => {
      this.slides.lockSwipes(true);
      let slideId = this.navParams.get('slideId');
      if (slideId) {
        this.previousSlide = 'EditProfile';
        this.title = 'Validate OTP';
        this.email = this.navParams.get('email');
        this.generatedOtp = this.navParams.get('generatedOtp');
        setTimeout(() => {
          this.slideTo(slideId, 'EditProfile');
        }, 500)
      }
    })
  }

  // enable footer button for respective slide
  isActiveSlide(number: any) {
    return number == this.slides.getActiveIndex();
  }

  onLogin() {
    this.helperService.showLoading();
    let tempEmail = this.toLowerCase(this.loginSlide.email);
    this.helperService.isUserExists(tempEmail)
      .then((user: any) => {
        if (user && user.length) {
          this.verifyUser();
        }
        else {
          this.helperService.hideLoading();
          this.toastMessage('Enter valid credentials');
        }
      })
  }

  verifyUser() {
    let self = this;
    this.af.auth.login({
      email: this.loginSlide.email,
      password: this.loginSlide.password
    })
      .then((firebaseUser: any) => {
        self.af.database.object('/users/' + firebaseUser.uid)
          .take(1)
          .subscribe((user: any) => {
            self.userData.login(user)
              .then(() => {
                this.helperService.hideLoading();
                self.events.publish('user:login');
                self.navCtrl.setRoot(NavigationDashboardPage);
              })
          })
      })
      .catch((err: any) => {
        this.helperService.hideLoading();
        this.helperService.showMessage('Enter valid credentials');
      })
  }

  forgotPassword() {

    this.helperService.showLoading();
    let tempEmail = this.toLowerCase(this.forgotPasswordSlide.email);

    this.helperService.isUserExists(tempEmail)
      .then((user: any) => {

        if (user && user[0] && user[0].isUserExists && user[0].status == 'accepted') {
          this.helperService.sendOtp(tempEmail, 'forgotPassword')
            .then((res) => {
              this.helperService.hideLoading();
              if (res.msg == 'success') {
                this.hideBackButton = false;
                this.generatedOtp = res.otp.slice(15, -7);
                this.title = 'Validate OTP';
                this.otp = '';
                this.slideTo(2, 'ForgotPassword');
              }
            });
        }
        else {
          this.helperService.hideLoading();
          this.toastMessage('Enter valid email address')
        }
      })
  }

  validateOtp() {
    // Show error for wrong otp entered
    if (this.generatedOtp != this.otp) {
      this.toastMessage('OTP entered is incorrect');
      return false;
    }
    else {
      this.toastMessage('OTP validated successfully');
      if (this.previousSlide == 'ForgotPassword') {
        this.title = 'Change Password';
        this.resetPasswordSlide = {};
        // show reset-password slide
        this.slideTo(3, null)
      }
      else if (this.previousSlide == 'EditProfile') {
        this.helperService.showLoading();
        this.userData.getUid()
          .then((uid) => {
            return this.helperService.updateEmail(uid, this.email)
          })
          .then(() => {
            this.helperService.hideLoading();
            this.title = 'Login';
            this.navCtrl.pop();
            this.helperService.showMessage('Email Updated sucessfully');
          })
          .catch((err) => {
            this.helperService.hideLoading();
            this.navCtrl.pop();
            this.helperService.showMessage('Failed to Update Email');
            console.log('Failed to Update Email', err);
          })
      }
    }
  }

  resendOtp() {

    this.helperService.showLoading();
    this.helperService.sendOtp(this.forgotPasswordSlide.email, 'forgotPassword')
      .then((res: any) => {
        this.helperService.hideLoading();

        if (res.msg == 'success') {
          this.toastMessage('OTP sent successfully');
          this.generatedOtp = res.otp.slice(15, -7);
        } else {
          console.log('err sending otp');
        }
      })
  }

  resetPassword() {
    this.helperService.showLoading();

    if (this.resetPasswordSlide.password.length != 6 || this.resetPasswordSlide.confirmPassword.length != 6) {
      this.helperService.hideLoading();
      this.helperService.showMessage('Password must be atleast 6 characters');
      return false;
    }

    if (this.resetPasswordSlide.password !== this.resetPasswordSlide.confirmPassword) {
      this.helperService.hideLoading();
      this.toastMessage('Passwords do not match');
      return false;
    }

    this.helperService.resetPassword(this.forgotPasswordSlide.email, this.resetPasswordSlide.password)
      .then(() => {
        this.helperService.hideLoading();
        this.toastMessage('Password updated successfully');
        this.hideBackButton = true;
        this.title = 'Login';
        this.loginSlide.password = '';
        this.slideTo(0, null);
      })
      .catch((errRes: any) => {
        if (errRes && errRes.error && errRes.error.code == 'auth/invalid-password') {
          this.helperService.showMessage('Password must be atleast 6 characters');
        }
      })
  }

  viewLoginSlide() {
    this.hideBackButton = true;
    this.title = 'Login';
    this.slideTo(0, null);
  }

  viewForgotPasswordSlide() {
    this.hideBackButton = false;
    this.title = 'Forgot Password';
    this.slideTo(1, null);
  }

  slideTo(index: number, previousSlide: any) {
    if (previousSlide) {
      this.previousSlide = previousSlide;
    }
    this.slides.lockSwipes(false);
    this.slides.slideTo(index);
    this.slides.lockSwipes(true);
  }

  toastMessage(message: any) {
    this.toastCtrl.create({
      message: message,
      duration: 2000
    }).present();
  }

  toLowerCase(email: any) {
    return email.toLowerCase();
  }

  ngOnDestroy() {
    this.menuCtrl.swipeEnable(true);
    this.menuCtrl.enable(true);
  }
}
