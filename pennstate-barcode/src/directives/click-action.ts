import { Directive, Input, Renderer, ElementRef } from '@angular/core';
import { App } from 'ionic-angular';
import { InAppBrowser } from 'ionic-native';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { NavigationDashboardPage } from '../pages/navigation-dashboard/navigation-dashboard';
import { CustomPage } from '../pages/custom-page/custom-page';
import { ModeratorList } from '../pages/moderator-list/moderator-list';
import { EventListPage } from '../pages/event-list/event-list';
import { NotificationsListPage } from '../pages/notifications/notification-list/notification-list';
import { PostsPage } from '../pages/posts/posts';
import { FeedbackRatingListPage } from '../pages/feedbacks-ratings/feedback-rating-list/feedback-rating-list';

@Directive({
  selector: '[clickAction]'
})

export class ClickActionDirective {

  @Input('clickAction') action: any;
  @Input('clickType') type: string;

  clickActionListening: boolean = false;
  components: any;

  constructor(
    private el: ElementRef,
    private renderer: Renderer,
    public app: App,
    public afoDatabase: AngularFireOfflineDatabase
  ) {
    this.components = {
      NavigationDashboardPage: NavigationDashboardPage,
      EventListPage: EventListPage,
      ModeratorList: ModeratorList,
      NotificationsListPage: NotificationsListPage,
      PostsPage: PostsPage,
      FeedbackRatingListPage:FeedbackRatingListPage
    };
  }

  ngOnChanges() {
    if (!this.action && this.clickActionListening) {
      return false;
    }
    this.clickActionListening = true;
    this.type = this.type || 'web';

    this.renderer.listen(this.el.nativeElement, 'click', () => {
      if (this.action && this.type == 'menu') {
        this.viewMenu();
      } else if (this.action && this.type) {
        switch (this.type) {
          case 'web':
          case 'website':
          case 'www':
            this.openBrowser(this.action);
            break;
          case 'tel':
          case 'call':
          case 'telephone':
            window.open('tel:' + this.action)
            break;
          case 'mailto':
          case 'mail':
            window.open('mailto:' + this.action, "_system")
            break;
        }
      }
    })
  }

  viewMenu() {
    if (this.action.type == 'PAGE') {
      this.app.getRootNav().setRoot(CustomPage, { pageId: this.action.value, title: this.action.title });
    } else if (this.action.type == 'LAUNCH_URL') {
      this.openBrowser(this.action.value);
    } else if (this.action.type == 'COMPONENT') {
      if(this.action.value == "ModeratorList"){
        this.app.getRootNav().setRoot(this.components[this.action.value]);
      }
      else{
        this.app.getRootNav().setRoot(this.components[this.action.value]), { title: this.action.title };
      }
    }
  }

  viewComponent() {
    this.app.getRootNav().setRoot(this.components[this.action.value], { title: this.action.title });
  }

  openBrowser(link: any) {
    if (link.indexOf('http') < 0) {
      link = 'http://' + link;
    }
    new InAppBrowser(link, '_blank', 'location=yes,closebuttoncaption=Done,toolbar=yes,presentationstyle=pagesheet,transitionstyle=fliphorizontal');
  }
}
