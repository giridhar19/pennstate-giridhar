import { Component, Input } from '@angular/core';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';

@Component({
  selector: 'user-card',
  templateUrl: 'user-card.html'
})
export class UserCard {
  @Input('params') args: any;

  user?: any = { avatarText: '' };
  uid: any;

  constructor(
    public afoDatabase: AngularFireOfflineDatabase
  ) { }

  ngOnChanges() {
    let self = this;
    self.afoDatabase.object('/users/' + this.args.uid)
      .subscribe((user: any) => {
      if(user.$exists()) {
        self.user = user;            
        if (user.status == 'accepted') {
          self.user.avatarText = [(user.firstName || ''), (user.lastName || '')].join(' ');
        } else {
          self.user.avatarText = user.email;
        }
      }
      });
  }
}
