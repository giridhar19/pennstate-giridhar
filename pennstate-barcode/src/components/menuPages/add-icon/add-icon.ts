import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';

@Component({
  selector: 'add-icon',
  templateUrl: 'add-icon.html'
})
export class AddIcon {
  icons: any = [];
  iconsList: any;
  searchInput: any = "";

  constructor(
    public viewCtrl: ViewController,
    public afoDatabase: AngularFireOfflineDatabase,
  ) {
    let temp: any = [];
    afoDatabase.list('/ionicons', { preserveSnapshot: true })
      .subscribe((snapshots: any) => {
        for (let snapshot of snapshots) {
          if (snapshot.$value && snapshot.$value.length) {
            temp.push(snapshot.$value);
          }
        }
        this.iconsList = Object.assign([], temp);
        this.icons = this.iconsList;
      });
  }

  searchIcons() {
    let self = this;
    if (this.searchInput.length > 0) {
      this.icons = this.iconsList.filter(function(result: any) {
        return result.indexOf(self.searchInput) >= 0;
      });
    }
    else {
      this.icons = this.iconsList;
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  addIcon(iconName: any) {
    this.viewCtrl.dismiss(iconName);
  }
}
