import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ModalController, LoadingController, ToastController, AlertController, ActionSheetController } from 'ionic-angular';
import { AngularFire } from 'angularfire2';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { AddMenu } from '../addMenu/addMenu'

@Component({
  selector: 'menu-pages',
  templateUrl: 'menu-pages.html'
})

export class MenuPages {
  @Input('params') args: any;
  @Output() updateMenu = new EventEmitter();

  pages: any[] = [];
  menuItems: any = [];
  isListExpanded: boolean = false;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController
  ) { }

  ngOnChanges() {
    new Promise((resolve) => {
      // list only published pages to add menu
      let tempPages: any = [];
      this.afoDatabase.list('/pages', { preserveSnapshot: true })
        .subscribe((pages) => {
          pages.forEach(function(page) {
            if (page.published) {
              tempPages.push(page);
            }
          });
          this.pages = Object.assign([], tempPages);
        });
      resolve();
    })
      .then(() => {
        new Promise((resolve) => {
          this.afoDatabase.list('/menus/' + this.args.menuName, { preserveSnapshot: true })
            .subscribe((snapshots) => {
              if (!snapshots.length) {
                return false;
              }
              snapshots.forEach(function(snapshot) {
                delete snapshot.$key;
                delete snapshot.$exists;
              })
              this.menuItems = Object.assign([], snapshots);
            });
          resolve();
        });
      })
      .then(() => {
        if (this.args.pageId) {
          this.afoDatabase.object('/pages/' + this.args.pageId)
            .take(1)
            .subscribe((page) => {
              if (page.title) {
                this.menuItems = page.pageFooter || [];
              }
            });
        }
      });
  }

  toggleList() {
    this.isListExpanded = !(this.isListExpanded);
  }

  showOptions(item: any, index: number) {
    let self: any = this;
    this.actionSheetCtrl.create({
      title: 'Options',
      buttons: [
        {
          text: 'Edit',
          icon: 'create',
          handler: () => {
            self.createMenu(item, index);
          }
        }, {
          text: 'Delete',
          icon: 'trash',
          handler: () => {
            self.deleteItem(index);
          }
        }, {
          text: 'Cancel',
           icon: 'close',
          role: 'cancel'
        }
      ]
    }).present();
  }

  onItemReorder(indexes: any) {
    let element = this.menuItems[indexes.from];
    this.menuItems.splice(indexes.from, 1);
    this.menuItems.splice(indexes.to, 0, element);
    this.updateMenu.emit({ menuItems: this.menuItems, menuName: this.args.menuName });
  }

  deleteItem(index: number) {
    this.menuItems.splice(index, 1);
    this.updateMenu.emit({ menuItems: this.menuItems, menuName: this.args.menuName });
  }

  deleteMenu() {
    let alert = this.alertCtrl.create({
      title: 'Confirmation',
      message: 'Are you sure to permanently delete this menu?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Continue',
          handler: () => {
            this.menuItems = [];
            this.af.database.object('/menus/' + this.args.menuName).set([]);
          }
        }
      ]
    });
    alert.present();
  }

  createMenu(menuItem: any, index: number) {
    if (this.args.menuName == 'HOME_PAGE' && this.menuItems.length >= 9) {
      let alert = this.alertCtrl.create({
        title: 'Menu Limit Exceeded',
        message: 'Cannot add more than 9 Menus',
        buttons: ['Dismiss']
      });
      alert.present();
    }
    else {

        let modal = this.modalCtrl.create(AddMenu,
          {
            data: menuItem,
            options: 'menuTypes',
            title: 'Menu Item',
            target: 'Menu',
            isMenu: true,
            isNotification: false
          });
        modal.present();

        modal.onDidDismiss((data: any) => {
          if (!data) {
            return false;
          }

          if (data && (index === undefined)) {
            this.menuItems.push(data);
          } else {
            this.menuItems[index] = data;
          }
          this.updateMenu.emit({ menuItems: this.menuItems, menuName: this.args.menuName });
        });
    }
  }
}
