import { Headers } from '@angular/http';

export class ApiConfigurations {

  ENV: any = 'firebase';
  servers: any = {
    'firebase': 'https://us-central1-eventiplex-with-campusmgmt.cloudfunctions.net/',
    'test': 'http://em-tal-ws8.em.ua-net.ua.edu/',
    'publicTest': 'http://crmtest.ua.edu/',
    'prod': 'http://192.168.0.109:3000/api',
    'local': '/assets/mock-data/',
  };
  constructor() {
  }

  getHeaders() {
    let headers = new Headers();
    headers.append('Content-Type', 'text/plain');
    headers.append('Username', 'talismaadmin');
    headers.append('Password', 't@l15man!!1');
    return headers;
  }

  public getEnv() {
    return this.ENV;
  }

  public getServerUrl() {
    return this.servers[this.getEnv()];
  }

}
