import { Injectable } from '@angular/core';
import { App, AlertController, Platform, ModalController, ToastController, LoadingController } from 'ionic-angular';
import { AngularFire } from 'angularfire2';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { Http, Headers } from '@angular/http';
import { Settings } from '../constants/firebase-config';
import { CookieService } from 'ngx-cookie';
import { Storage } from '@ionic/storage';
import { Device } from '@ionic-native/device';
import { EmailComposer } from '@ionic-native/email-composer';
import * as moment from 'moment-timezone';
import { ModeratorAlert } from '../pages/moderator-alert/moderator-alert';
import * as _ from 'underscore';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

@Injectable()
export class HelperService {
  loaderInstance: any = null;
  timeZone: any = 'America/Matamoros';
  eventAlertInstance: any = null;
  oldEventAlertData: any = {};

  constructor(
    public alertCtrl: AlertController,
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public app: App,
    private cookieService: CookieService,
    public email: EmailComposer,
    public http: Http,
    public modal: ModalController,
    public platform: Platform,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public device: Device,
    public toastCtrl: ToastController
  ) { }

  getUid(): Promise<string> {
    return this.storage.get('uid')
      .then((value) => {
        return value;
      })
  }

  getRoleValue(): Promise<number> {
    return this.storage.get('roleValue')
      .then((value: any) => {
        return value;
      });
  }

  // show Loading UI
  showLoading() {
    this.loaderInstance = this.loadingCtrl.create();
    this.loaderInstance.present();
    // return this.loaderInstance;
  }

  // hide the Loading UI
  hideLoading() {
    if (this.loaderInstance !== null) {
      this.loaderInstance.dismiss();
      this.loaderInstance = null;
    }
  }

  showMessage(msg: any, duration?: any) {
    this.toastCtrl.create({
      message: msg,
      duration: duration || 2000
    }).present();
  }

  getHeaders() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
  }

  isCordovaEnabled() {
    return (this.device.cordova !== null);
  }

  getTimeStamp() {
    return moment().tz(this.timeZone).subtract(1, 'days').endOf('day').format('');
  }

  sendOtp(email: string, type: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getHeaders();
      let headers = this.getHeaders();
      let body = JSON.stringify({ email: email.toLowerCase(), type: type });
      this.http.post(Settings.cloudFunctionURL + '/sendOtp', body, headers)
        .map((res: any) => res.json())
        .subscribe((data) => {
          if (data && data.message && data.message.includes('error')) {
            reject(data.message);
          }
          else {
            resolve({ msg: data.message, otp: data.otp });
          }
        });
    })
  }

  isAdmin() {
    return new Promise((resolve, reject) => {
      this.getRoleValue()
        .then((roleValue) => {
          if (roleValue == 99) {
            resolve()
          }
          else {
            this.alertCtrl.create({
              title: 'Warning',
              subTitle: 'You\'re not authorized to perform the selected operation',
              buttons: ['Dismiss']
            }).present();
            reject('Not enough privileges to perform action');
          }
        })
    })
  }

  convertFromUtcToTz(datetime: any) {
    datetime = datetime || '';
    return moment.utc(datetime).tz(this.timeZone);
  }

  //generates unique
  generateUUID(): Promise<{ uuid: string }> {
    return new Promise((resolve, reject) => {
      let d = new Date().getTime();
      if (window.performance && typeof window.performance.now === "function") {
        d += performance.now();; //use high-precision timer if available
      }
      let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
      });
      resolve({ uuid: uuid })
    })
  }

  showEventAlert(message: any, enableEventView: any) {
    return new Promise((resolve, reject) => {
      let eventData = message.additionalData.event || {};
      console.log(eventData);
      let type = enableEventView == 'true' ? 'ASSIGNED' : 'REVOKED';
      if (!!this.eventAlertInstance && _.isEqual(this.oldEventAlertData, { eventId: eventData.eventId, eventName: eventData.eventName, type: type })) {
        resolve({ status: "DISMISS" });
      }
      else {
        if (!!this.eventAlertInstance) {
          console.log("- - - Dismissed - - -");
          this.eventAlertInstance.dismiss();
        }
        this.oldEventAlertData = { eventId: eventData.eventId, eventName: eventData.eventName, type: type };

        this.eventAlertInstance = this.modal.create(ModeratorAlert, {
          data: {
            title: message.title,
            message: message.message,
            eventName: eventData.eventName,
            enableEventView: enableEventView,
          }
        }, { showBackdrop: true });

        this.eventAlertInstance.onDidDismiss((data: any) => {
          this.eventAlertInstance = null;
          data && data == 'GOTO_EVENT' ? resolve({ status: "GOTO_EVENT", eventId: eventData.eventId }) : resolve({ status: "DISMISS" });

        });
        this.eventAlertInstance.present();
      }
    })
  }

  resetPassword(email: string, password: string): Promise<{ msg: any }> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let cred = { email: email.toLowerCase(), password: password }
      let body = JSON.stringify(cred);
      this.http.post(Settings.cloudFunctionURL + '/resetPassword', body, headers)
        .map((res: any) => res.json())
        .subscribe((data: any) => {
          if (data && data.message == 'success') {
            resolve(data);
          } else {
            reject(data);
          }
        });
    });
  }

  createUser(user: any): Promise<string> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify(user);
      this.http.post(Settings.cloudFunctionURL + '/createUser', body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res: any) => {
          if (res && res.message == 'success') {
            resolve(res);
          } else {
            console.log('reject user creation ', res);
            reject(res);
          }
        });
    });
  }

  isUserExists(email: string): Promise<any> {
    return new Promise((resolve) => {
      this.af.database.list('users', { query: { orderByChild: 'email', equalTo: email.toLowerCase() } })
        .subscribe((snapshots: any) => {
          let users: any[] = [];
          if (snapshots && snapshots.length) {
            _.each(snapshots, function(snapshot: any) {
              if (snapshot.appName == 'ADMIN') {
                delete snapshot.$exists;
                delete snapshot.$key;
                snapshot.isUserExists = true;
                users.push(snapshot);
              }
            })
          }
          resolve(users);
        });
    });
  }

  sendEventEmail(usersArray: any, data: any) {
    let headers = this.getHeaders();
    let body = JSON.stringify({ data: data, usersArray: usersArray });
    this.http.post(Settings.cloudFunctionURL + '/sendEventEmail', body, { headers: headers })
      .subscribe((res) => {
        console.log(res)
      })
  }

  sendNotification(message: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify({ message: message })
      this.http.post(Settings.cloudFunctionURL + '/sendNotifications', body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          console.log(res)
          resolve(res)
        })
    })
  }

  flushUser(uid: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify({ uid: uid })
      this.http.post(Settings.cloudFunctionURL + '/flushUser', body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          console.log(res)
          resolve(res)
        })
    })
  }

  flushUserData(uid: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify({ uid: uid })
      this.http.post(Settings.cloudFunctionURL + '/flushUserData', body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          console.log(res)
          resolve(res)
        })
    })
  }

  subscribeToEventNotifications(session: any, uid: string) {
    return new Promise((resolve, reject) => {
      this.af.database.object('topics/' + session.id + '/subscribers/' + uid)
        .set(true)
        .then(() => {
          this.af.database.list('/notificationTargets', { query: { orderByChild: 'eventId', equalTo: session.id } })
            .take(1)
            .subscribe((res) => {
              if (!res.length) {
                this.af.database.list('notificationTargets')
                  .push({})
                  .set({ name: session.name, eventId: session.id })
                  .then(() => {
                    resolve('sucess');
                  })
                  .catch((err) => {
                    console.log('error occured', err);
                    reject(err);
                  });
              }
              else {
                resolve('sucess');
              }
            });
        })
        .catch((err) => {
          console.log('error occured', err);
          reject(err);
        });
    })
  }

  unSubscribeToEventNotifications(session: any, uid: string) {
    return new Promise((resolve, reject) => {
      this.af.database.object('topics/' + session.id + '/subscribers/' + uid)
        .remove()
        .then(() => {
          resolve();
        })
        .catch((err) => {
          reject(err);
          console.log('error occured', err);
        });
    })
  }

  updateEmail(uid: string, email: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let headers = this.getHeaders();
      let body = JSON.stringify({ uid: uid, email: email })
      this.http.post(Settings.cloudFunctionURL + '/updateEmail', body, { headers: headers })
        .map((res: any) => res.json())
        .subscribe((res) => {
          console.log(res)
          resolve(res)
        })
    })
  }
}
