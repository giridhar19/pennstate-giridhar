import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { ApiConfigurations } from './apiConfig';
import * as _ from 'underscore';
import { HelperService } from '../helperService';

@Injectable()
export class ApiService {

  constructor(
    public helperService: HelperService,
    public http: Http,
    public config: ApiConfigurations
  ) { }

  getUrl(endPoint: any, type?: string) {

    if (this.isLocalProvider()) {
      endPoint = endPoint + '.json';
    }
      return this.config.getServerUrl() + '' + endPoint;
  }

  urlWithFilter(request: any) {
    return request.url + '?$filter=' + request.key + ' eq ' + request.value;
  }

  parseRequest(request: any) {
    let keys: any = Object.keys(request);
    keys.forEach((key: any) => {
      if (key != 'url') {
        request.key = key;
        request.value = request[key];
      }
    });
    return request;
  }

  isLocalProvider() {
    return this.config.getEnv() == 'local';
  }

  ajax(url: any, returnType: string) {
    return new Promise((resolve, reject) => {
      this.http.get(url, { headers: this.config.getHeaders(), method: "GET" })
        .subscribe((res: any) => {
          res = returnType == 'object' ? res.json() : res;
          if (res) {
            resolve(returnType !== 'object' || this.isLocalProvider() ? res : res.value);
          } else {
            resolve([]);
          }
        },
        (err: any) => {
          this.helperService.showMessage('Error occured, please try after some time');
          this.helperService.hideLoading();
          resolve([]);
        }
        );
    });
  }

  filterData(data: any, options: any) {
    return new Promise((resolve) => {
      let temp: any;
      temp = _.findWhere(data, { [options.key]: options.value }) ||
        _.findWhere(data, { [options.key]: options.value });
      resolve(temp);
    });
  }

  public find(endPoint: any) {
    return this.ajax(this.getUrl(endPoint), 'object');
  }

  public findBy(request: any) {
    if (this.isLocalProvider()) {
      return this.ajax(this.getUrl(request.url), 'object')
        .then((res: any) => {
          return this.filterData(res, this.parseRequest(request));
        });
    } else {
      request.url = this.getUrl(request.url);
      return this.ajax(request.url, 'object');
    }
  }
}
