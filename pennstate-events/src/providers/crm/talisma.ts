import { Injectable } from '@angular/core';
import { Events } from './events';
import { Contacts } from './contacts';
import { Participants } from './participants';
import { Speakers } from './speakers';

@Injectable()
export class Talisma {
  constructor(
    public events: Events,
    public participants: Participants,
    public speakers: Speakers,
    public contacts: Contacts
  ) { }
}
