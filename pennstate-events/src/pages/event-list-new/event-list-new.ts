import { Component, ViewChild } from '@angular/core';
import { Content, NavController, NavParams, Searchbar } from 'ionic-angular';
import { ApiService } from '../../providers/crm/apiService';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { HelperService } from '../../providers/helperService';
import { Talisma } from '../../providers/crm/talisma';
import { EventDetailPage } from '../event-detail/event-detail';
import * as moment from 'moment';
import * as _ from 'underscore';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'page-event-list-new',
  templateUrl: 'event-list-new.html'
})
export class CalenderEventListPage {

  @ViewChild(Content) eventList: Content;

  events: any[] = [];
  segment: any = 'live';
  live: any = [];
  past: any = [];
  uid: any;
  allEvents: any = [];
  loadedEvents: any = [];
  loadedEventsCount: number = 0;
  loadedLiveEventsCount: number = 0;
  loadedPastEventsCount: number = 0;
  eventsPerPage: number = 10;
  timeZone: string;
  reachedMaxLimit: boolean = false;
  query: string = '';
  hideSearchBar: boolean = true;
  eventIds: any[] = [];
  searchEnabled: boolean = false;
  subURI: string = '';
  title: string;
  searchTerm = new Subject<string>();
  moment = moment;
  _ = _;

  @ViewChild('searchBar') searchBar: Searchbar;

  constructor(
    public helperService: HelperService,
    private _nav: NavController,
    private navparams: NavParams,
    public apiService: ApiService,
    public afoDatabase: AngularFireOfflineDatabase,
    public talisma: Talisma
  ) {
    this.title = this.navparams.get('title') ? this.navparams.get('title') : "Events";
    new Promise((resolve) => {
      this.afoDatabase.object('/settings/timeZone')
        .take(1)
        .subscribe((zone: any) => {
          this.timeZone = zone.$value;
          resolve();
        });
    })
      .then(() => {
        this.runSearchObservable();
        this.init();
      });
  }

  runSearchObservable() {
    this.searchTerm
      .debounceTime(1000)
      .distinctUntilChanged()
      .switchMap((res) => {
        this.helperService.showLoading();
        this.loadedEvents = [];
        this.reachedMaxLimit = false;
        this.loadedLiveEventsCount = 0;
        this.loadedPastEventsCount = 0;
        this.searchEnabled = true;
        this.subURI = " and contains(Name,'" + this.query + "')";
        return this.getEvents(this.segment, this.subURI);
      })
      .subscribe((events) => {
        this.convertToTz(events)
          .then((events: any) => {
            this.loadedEvents = events;
            this.helperService.hideLoading();
          });
      })
  }

  init(ionRefresher?: any) {
    if (!ionRefresher) {
      this.helperService.showLoading();
    }
    new Promise((resolve) => {
      let temp = { loadedLiveEventsCount: this.loadedLiveEventsCount, loadedPastEventsCount: this.loadedPastEventsCount, loadedEvents: this.loadedEvents };
      this.loadedLiveEventsCount = 0;
      this.loadedPastEventsCount = 0;
      this.getAllEvents()
        .then(() => {
          if (ionRefresher) {
            ionRefresher.complete();
          }
          else {
            this.helperService.hideLoading();
          }
        })
        .catch((err) => {
          console.log('err occured', err);
          this.helperService.hideLoading();
          this.loadedLiveEventsCount = temp.loadedLiveEventsCount;
          this.loadedPastEventsCount = temp.loadedPastEventsCount;
          this.convertToTz(temp.loadedEvents)
            .then((pastEvents: any) => {
              this.loadedEvents = pastEvents;
            });
        });
    });
  }

  convertToTz(someEvents: any) {
    return new Promise((resolve) => {
      someEvents.forEach((event: any) => {
        event.EventStartDate = moment(event.EventStartDate).tz(this.timeZone);
      });
      resolve(someEvents);
    })
  }

  onIonClear() {
    this.query = '';
    this.searchEnabled = false;
    this.reachedMaxLimit = false;
    this.loadedEvents = [];
    this.loadedEventsCount = 0;
    this.toggleNavBar();
    this.loadEvents();
  }

  toggleNavBar() {
    this.hideSearchBar = !this.hideSearchBar;
    setTimeout(() => {
      if (this.searchBar) {
        this.searchBar.setFocus();
      }
    })
  }

  getAllEvents() {
    return new Promise((resolve) => {
      this.getEvents(this.segment)
        .then((response: any) => {
          if (response.length) {
            this.convertToTz(response)
              .then((events: any) => {
                console.log(events);
                if (this.segment == 'live') {
                  this.live = events;
                  this.loadedEvents = events;
                }
                else {
                  this.past = events;
                  this.loadedEvents = events;
                }
              })
          }
          resolve();
        });
    });
  }

  updateList(segment: any) {
    this.reachedMaxLimit = false;
    this.hideSearchBar = true;
    this.query = '';
    if (segment.value == 'live') {
      this.loadedEvents = this.live;
      this.loadedLiveEventsCount = this.loadedEvents.length;
    }
    else if (segment.value == 'past') {
      this.helperService.showLoading();
      this.getEvents('past')
        .then((events: any) => {
          this.convertToTz(events)
            .then((pastEvents: any) => {
              this.past = pastEvents;
              this.loadedPastEventsCount = pastEvents.length;
              this.loadedEvents = this.past;
              this.helperService.hideLoading();
            });
        })
        .catch((err) => {
          this.helperService.hideLoading();
          console.log('err occured', err);
        });
    }
    this.eventList.scrollToTop();
  }

  loadEvents(infiniteScroll?: any) {
    this.helperService.hideLoading();
    setTimeout(() => {
      new Promise((resolve) => {
        this.helperService.showLoading();
        this.getEvents(this.segment, this.searchEnabled ? this.subURI : '')
          .then((events: any) => {
            this.convertToTz(events)
              .then((events: any) => {
                this.loadedEvents = this.loadedEvents.concat(events);
                if (infiniteScroll) {
                  infiniteScroll.complete();
                }
                this.helperService.hideLoading();
              });
          });
      });
    }, 100);
  }

  groupByDate(events: any[]) {
    return _.groupBy(events, (event: any) => { return moment(event.EventStartDate).format('YYYYMMDD') })
  }

  groupByMonth(events: any[]) {
    return _.groupBy(events, (event: any) => { return moment(event.EventStartDate).format('YYYY-MMM') })
  }

  viewEvent(eventData: any) {
    this._nav.push(EventDetailPage, { event: eventData });
  }

  onInput() {
    this.helperService.hideLoading();
    if (this.query && this.query.length) {
      this.searchTerm.next(this.query);
    }
    else {
      this.reachedMaxLimit = false;
      this.searchEnabled = false;
      this.loadedEvents = [];
      this.loadEvents();
    }
  }

  getEvents(segment: string, query?: string) {
    return new Promise((resolve, reject) => {
      let subUrl = '$filter=(EventStartDate' + (segment == 'live' ? ' ge ' : ' lt ') + (moment().tz(this.timeZone).subtract(1, 'days').endOf('day').format('') + ") ");
      subUrl = subUrl.concat(((query && query.length) ? "" + query + "&" : "&") + '$orderby=EventStartDate' + (segment == 'live' ? ' asc' : ' desc '));
      this.talisma.events.getEvents(this.eventsPerPage, segment == 'live' ? this.loadedLiveEventsCount : this.loadedPastEventsCount, subUrl)
        .then((response: any) => {
          if (response.length) {
            segment == 'live' ? this.loadedLiveEventsCount += response.length : this.loadedPastEventsCount += response.length
            resolve(response)
          }
          else {
            this.reachedMaxLimit = true;
            resolve([]);
          }
        })
        .catch((err) => {
          console.log('err occured', err);
          reject(err)
        });
    });
  }

  getClass(event: any) {
    return ((moment(event.EventStartDate).tz(this.timeZone).format('x')) >= (moment().tz(this.timeZone).subtract(1, 'days').endOf('day').format('x'))) ? '' : 'past-event';
  }
}
