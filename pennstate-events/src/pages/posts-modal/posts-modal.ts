import { Component } from '@angular/core';
import { AlertController, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { FirebaseAnalyticsProvider } from '../../providers/firebase-analytics-provider';
import { AngularFireOfflineDatabase } from 'angularfire2-offline';

@Component({
  selector: 'posts-modal',
  templateUrl: 'posts-modal.html',
  providers: [FirebaseAnalyticsProvider]
})
export class PostModalPage {

  post: any = {};
  postTypes: any = {};
  constructor(
    public alertCtrl: AlertController,
    public afoDatabase: AngularFireOfflineDatabase,
    public firebaseAnalytics: FirebaseAnalyticsProvider,
    public navParams: NavParams,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public viewCtrl: ViewController
  ) {
    this.afoDatabase.list('/settings/postTypes')
      .subscribe((types) => {
        this.postTypes = types;
        types.forEach((type) => {
          this.postTypes[type.$key] = type.$value;
        })
        this.post = this.navParams.get('post') ? this.navParams.get('post') : {};
      })
  }

  ionViewDidEnter() {
    this.firebaseAnalytics.setScreen('PostModal');
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
