import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AngularFireOfflineDatabase } from "angularfire2-offline";
import { AngularFire } from "angularfire2";
import { App, LoadingController, NavController, MenuController, ModalController, Platform, ToastController } from "ionic-angular";
import { OtpPage } from "../otp/otp";
import { UserData } from "../../providers/user-data";
import { HelperService } from "../../services/services";
import { Keyboard } from "@ionic-native/keyboard";

@Component({
  selector: "page-user",
  templateUrl: "login.html",
  providers: [HelperService]
})
export class LoginPage {
  user: any;
  submitted = false;
  login: any = {};
  bannerPicture: string;
  showFooter: boolean = true;

  constructor(
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public app: App,
    public helperService: HelperService,
    public keyboard: Keyboard,
    public loadingCtrl: LoadingController,
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public platform: Platform,
    public toastCtrl: ToastController,
    public userData: UserData,
    public modalCtrl: ModalController
  ) {
    this.menuCtrl.enable(false);
    // homesliders url from db
    this.afoDatabase.object("/settings/homePage", { preserveSnapshot: true }).subscribe((snapshot: any) => {
      this.bannerPicture = snapshot ? snapshot[0] : "";
    });
    // hide footer on keyboard show
    this.keyboard.onKeyboardShow().subscribe(() => {
      this.showFooter = false;
    });

    // show footer on keyboard hide
    this.keyboard.onKeyboardHide().subscribe(() => {
      this.showFooter = true;
    });

    //sets the FCM token to DB once the user loggedIn
    this.af.auth.subscribe((auth) => {
      if (auth) {
        let uid = auth.uid;
        this.userData.getFCMToken().then((token) => {
          if (token) {
            this.af.database
              .object("/notificationTokens/" + uid)
              .set({ token: token, type: this.platform.is("android") && !this.platform.is("core") ? "android" : "ios" });
          }
        });
      }
    });
  }

  onLogin(form: NgForm) {
    this.submitted = true;
    let self: any = this;
    if (form.valid) {
      let loading = this.loadingCtrl.create();
      loading.present();
      this.login.email = this.login.email.toLowerCase();
      this.afoDatabase
        .list("/users", { query: { orderByChild: "email", equalTo: this.login.email } })
        .take(1)
        .subscribe((snapshots) => {
          let user = snapshots[0];
          if (user && user.disabled) {
            let toast = this.toastCtrl.create({
              message: "User has been blocked by the Admin, please contact Admin for details",
              duration: 1500
            });
            loading.dismiss();
            toast.present();
          } else {
            this.helperService
              .sendOtp(this.login.email, "VerifyAccount")
              .then((res) => {
                if (res.msg == "success") {
                  loading.dismiss();
                  if (!self.platform.is("core")) {
                  }
                  self.navCtrl.push(OtpPage, { user: { email: self.login.email, generatedOtp: res.otp, uid: user ? user.uid : undefined } });
                } else {
                  let toast = self.toastCtrl.create({
                    message: "Failed to send the otp, please try after some time",
                    duration: 1500
                  });
                  loading.dismiss();
                  console.log("Failed to send the otp", res);
                  toast.present();
                }
              })
              .catch((err) => {
                console.log("err occured", err);
              });
          }
        });
    }
  }
}
