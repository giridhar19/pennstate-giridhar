import { Component } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { NgForm } from '@angular/forms';
import { LoginPage } from '../../login/login';
import { NavigationDashboardPage } from '../../navigation-dashboard/navigation-dashboard';
import { App, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { FirebaseAnalyticsProvider } from '../../../providers/firebase-analytics-provider';
import { UserData } from '../../../providers/user-data';
import { HelperService } from '../../../services/services';
import { AngularFireOfflineDatabase } from 'angularfire2-offline';

@Component({
  selector: 'page-user-datail-form',
  templateUrl: 'user-detail-form.html',
  providers: [FirebaseAnalyticsProvider, HelperService]
})

export class UserDetailFormPage {
  user: any = {};
  uid: string = '';
  roleValue: any;

  constructor(
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public app: App,
    public events: Events,
    public firebaseAnalytics: FirebaseAnalyticsProvider,
    public helperService: HelperService,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public userData: UserData
  ) {
    firebaseAnalytics.setScreen('UserDetailsForm')
    this.uid = this.navParams.get('uid');
    this.afoDatabase.object('users/' + this.uid)
      .subscribe((user) => {
        if (user.$exists()) {
          this.user = user;
        }
      })
  }

  checkFormValid(form: NgForm) {
    return form.invalid;
  }

  saveUserDetails(form: NgForm) {
    let loading = this.loadingCtrl.create({
      content: 'Saving the Data, Please Wait...'
    });
    loading.present();
    if (form.valid) {
      this.user.registrationComplete = true;
      this.af.database.object('users/' + this.uid)
      .update(this.user)
        .then(() => {
          loading.dismiss();
          this.firebaseAnalytics.setUser(this.user)
          return this.userData.login(this.user)
        })
        .then(() => {
          this.events.publish('user:login');
          return this.app.getRootNav().setRoot(NavigationDashboardPage)
        })
        .then(() => {
          this.toastCtrl.create({
            message: 'user details have been updated',
            duration: 3000
          }).present();
        })
        .catch((error) => {
          console.log('failed to update the user details' + error)
          this.toastCtrl.create({
            message: 'failed to update the user details',
            duration: 1500
          }).present();
        })
    }
  }

  cancelRegistration() {
    this.navCtrl.setRoot(LoginPage)
  }

}
