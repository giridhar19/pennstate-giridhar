import { Component } from "@angular/core";
import { AlertController, ModalController, NavParams } from "ionic-angular";
import { HelperService } from "../../providers/helperService";
import { UserData } from "../../providers/user-data";
import { AngularFireOfflineDatabase } from "angularfire2-offline";
import * as moment from "moment-timezone";
import { Talisma } from "../../providers/crm/talisma";
import { EventTicket } from "../../components/event-ticket/event-ticket";

@Component({
  selector: "page-event-detail",
  templateUrl: "event-detail.html"
})
export class EventDetailPage {
  event: any;
  username: any;
  uid: any;
  attendingList: any = [];
  timeZone: any;
  participationStatus: string = "";
  isEventRateable: boolean;
  participantId: any;
  image: any = "https://firebasestorage.googleapis.com/v0/b/eventiplex-with-campusmgmt.appspot.com/o/homepage%2Fhome-page-slider-1.jpg?alt=media&token=d8f82f66-2734-4969-9da9-2fc6a00e1699";

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public alertCtrl: AlertController,
    public helperService: HelperService,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public userData: UserData,
    public talisma: Talisma
  ) {
    this.helperService.showLoading();
    new Promise((resolve) => {
      this.event = this.navParams.get("event");
      this.event.RegisteredParticipants = this.event.RegisteredParticipants || 0;
      this.afoDatabase
        .object("settings/timeZone")
        .take(1)
        .subscribe((zone: any) => {
          this.timeZone = zone.$value;
          resolve();
        });
    })
      .then(() => {
        return this.userData.getUid();
      })
      .then((uid: any) => {
        this.uid = uid;
        return this.userData.getUsername();
      })
      .then((username) => {
        this.username = username; // temp call
        this.getStatus();
        this.helperService.hideLoading();
      })
      .catch((err: any) => {
        this.helperService.hideLoading();
        console.log("err occured", err);
      });

    this.afoDatabase
      .list("/participants/" + this.event.EventId, { query: { orderByChild: "status", equalTo: "Registered" } })
      .subscribe((participants: any) => {
        this.afoDatabase.object("/events/" + this.event.EventId + "/RegisteredParticipants").set(participants.length);
      });
  }

  // hide event options for past events
  isLive() {
    this.timeZone = this.timeZone || "America/Matamoros";
    return moment(this.event.EventStartDate)
      .tz(this.timeZone)
      .format("x") >=
      moment()
        .tz(this.timeZone)
        .subtract(1, "days")
        .endOf("day")
        .format("x")
      ? true
      : false;
  }

  // hide map if event shall be conducted 'online'
  isValidAddress() {
    let venue: string = this.event.EventVenue ? this.event.EventVenue.toLowerCase() : "";
    if (!venue || venue.includes("online")) {
      return true;
    }
  }

  getStatus() {
    let self: any = this;
    return new Promise((resolve) => {
      // this.talisma.participants.getParticipationStatus(this.event.EventId, this.uid)
      this.talisma.participants.getFirebaseParticipationStatus(this.event.EventId, this.uid).then((result: any) => {
        if (result && result.$exists()) {
          self.participationStatus = result.status;
        }
        resolve();
      });
    });
  }

  markInterested() {
    this.helperService.showLoading();

    // (this.event.EventId, this.uid)
    let temp: any = {
      name: this.username,
      uid: this.uid,
      status: "Interested"
    };
    this.afoDatabase
      .object("/participants/" + this.event.EventId + "/" + this.uid)
      .update(temp)
      // .then((result: any) => {
      //   return this.helperService.validateParticipant(result);
      // })
      // .then((participantId: any) => {
      //   this.participantId = participantId;
      // })
      // .then((result: any) => {
      //   return this.helperService.validateStatusUpdate(result);
      // })
      //   return this.getStatus();
      .then(() => {
        return this.updateParticipant("Interested");
      })
      .then(() => {
        // temp call
        this.subscribeToNotifications();
        return this.markEventAs("Interested");
      })
      .then(() => {
        this.helperService.hideLoading();
        this.helperService.showMessage("You have marked event as Interested", 2000, "showicon");
      })
      .catch((err: any) => {
        this.helperService.hideLoading();
        console.log("err ", err);
      });
  }

  // update Participant status as Interested or Canceled
  updateParticipant(status: string) {
    let temp: any = {
      name: this.username,
      uid: this.uid,
      status: status
    };
    return this.afoDatabase
      .object(`/participants/${this.event.EventId}/${this.uid}`)
      .update(temp)
      .then(() => {
        return this.getAttendedParticipant();
      })
      .then((count: any) => {
        this.participationStatus = status;
        return this.setAttendedParticipants(count);
      });
    // return this.talisma.participants.updateParticipant(this.event.EventId, this.participantId, status);
  }

  getAttendedParticipant() {
    return new Promise((resolve) => {
      this.afoDatabase
        .list("/participants/" + this.event.EventId, { query: { orderByChild: "status", equalTo: "Attended" } })
        .subscribe((attendedParticipants: any) => {
          resolve(attendedParticipants.length);
        });
    });
  }

  // set AttendedParticipants in 'event' node
  setAttendedParticipants(count: any) {
    return this.afoDatabase.object("/events/" + this.event.EventId).update({ AttendedParticipants: count });
  }

  updateRegisteredParticipants() {
    return this.afoDatabase.object("events/" + this.event.EventId).update({ RegisteredParticipants: this.event.RegisteredParticipants });
  }

  cancelTicket() {
    this.helperService.showLoading();
    this.updateParticipant("Cancelled")
      // .then((result: any) => {
      //   return this.helperService.validateStatusUpdate(result)
      // })
      // .then(() => {
      //   return this.getStatus();
      // })
      .then(() => {
        return this.updateRegisteredParticipants();
      })
      .then(() => {
        // temp call
        return this.markEventAs("Canceled");
      })
      .then(() => {
        return this.afoDatabase.object("/events/" + this.event.EventId + "/" + "RegisteredParticipants").set(--this.event.RegisteredParticipants);
      })
      .then(() => {
        this.helperService.hideLoading();
        this.helperService.showMessage("Event ticket canceled successfully ", 2000, "showicon");
      })
      .catch((err: any) => {
        this.helperService.hideLoading();
        console.log("err ", err);
      });
  }

  buyTicket() {
    let cost = this.event.EventCost ? "$" + this.event.EventCost : "$0";
    let alert = this.alertCtrl.create({
      title: "Payment confirmation",
      message: "You will be charged " + cost + " for this event ticket",
      buttons: [
        {
          text: "Cancel",
          role: "cancel"
        },
        {
          text: "Buy",
          handler: () => {
            this.helperService.showLoading();
            this.markEventAs("Registered")
              .then(() => {
                return this.updateParticipant("Registered");
              })
              .then(() => {
                return this.updateRegisteredParticipants();
              })
              .then(() => {
                return this.afoDatabase
                  .object("/events/" + this.event.EventId + "/" + "RegisteredParticipants")
                  .set(++this.event.RegisteredParticipants);
              })
              .then(() => {
                this.subscribeToNotifications();
                this.helperService.hideLoading();
                this.helperService.showMessage("Event ticket purchased successfully ", 2000, "showicon");
              })
              .catch((err: any) => {
                console.log(err);
              });
          }
        }
      ]
    });
    alert.present();
  }

  // temp fix
  // mark participationStatus as 'Registered, Canceled, Interested'
  markEventAs(status: any) {
    return this.afoDatabase.object("/users/" + this.uid + "/participationStatus/" + this.event.EventId).set(status);
  }

  // temp fix
  // get ticket purchased status
  shallowPurchase() {
    this.afoDatabase.object("/users/" + this.uid + "/participationStatus/" + this.event.EventId).subscribe((status: any) => {
      if (status.$value) {
        this.participationStatus = status.$value;
      }
    });
  }

  subscribeToNotifications() {
    this.helperService
      .subscribeToEventNotifications({ id: this.event.EventId, name: this.event.Name }, this.uid)
      .then((response) => { })
      .catch((err) => {
        console.log("err occured", err);
      });
  }

  viewTicket(event: any, eventData: any) {
    event.stopPropagation();
    console.log("eventData.EventId", eventData.EventId);
    let modal = this.modalCtrl.create(EventTicket, { eventId: eventData.EventId }, { enableBackdropDismiss: true });
    modal.present();
  }
}
