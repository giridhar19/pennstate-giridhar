import { Component, OnInit } from '@angular/core';
import { ViewController, NavParams, ToastController } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import * as moment from 'moment-timezone';

@Component({
  selector: 'rating-modal',
  templateUrl: 'rating-modal.html'
})

export class RatingModal implements OnInit {
  icons: any = [];
  user: any;
  feedback: any;
  message: any = "";
  uid: any;
  rate: any;
  eventId: any;
  type: any;
  ratingValue: any;
  placeholder: string;
  username: string;

  constructor(
    public viewCtrl: ViewController,
    public afoDatabase: AngularFireOfflineDatabase,
    public params: NavParams,
    public toastCtrl: ToastController
  ) {
  }

  ngOnInit() {
    this.uid = this.params.get('uid');
    this.ratingValue = this.params.get('ratingValue');
    this.username = this.params.get('username');
    this.eventId = this.params.get('eventId');
    this.placeholder = "Give feedbacks and share your experience about this event";
    this.afoDatabase.object('users/' + this.uid)
      .take(1)
      .subscribe((user) => {
        this.user = user;
        this.afoDatabase.object('/feedbacks/' + this.eventId, { preserveSnapshot: true })
          .take(1)
          .subscribe((feedback) => {
            this.feedback = feedback;
            if (this.feedback.$value === null) {
              this.feedback = {}
            }
          })
      });
    this.updateStars();
  }

  dismiss(status: boolean) {
    this.viewCtrl.dismiss(!!status);
  }

  updateRateValue(i: number) {
    this.ratingValue = i;
    this.updateStars();
  }

  updateStars() {
    for (let i = 0; i < 5; i++) {
      this.icons[i] = 'star-outline'
    }
    for (let i = 0; i <= this.ratingValue; i++) {
      this.icons[i] = 'star';
    }
  }

  submit() {
    if (!this.feedback[this.uid]) {
      this.feedback[this.uid] = { uid: this.uid, name: this.username, eventId: this.eventId, message: this.message, rating: this.ratingValue + 1, createdDate: moment.utc().format('x') }
    }
    this.afoDatabase.object('/feedbacks/' + this.eventId + '/' + this.uid)
      .set(this.feedback[this.uid])
      .then(() => {
        this.toastCtrl.create({
          message: 'Thank you for submitting your feedback',
          duration: 3000
        }).present();
        this.dismiss(true);
      })
      .catch((err) => {
        this.toastCtrl.create({
          message: 'Try again, failed to save',
          duration: 3000
        }).present();
      })
  }
}
