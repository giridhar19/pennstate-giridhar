import { Component, ComponentFactoryResolver, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { ActionSheetController, AlertController, App, ModalController, NavController, Platform, ToastController } from 'ionic-angular';
import { PostDirective } from './post.directive';
import { PostComponent } from './post.component';
import { PostService } from './post.service';
import { AngularFireOfflineDatabase } from 'angularfire2-offline';
import { AngularFire } from 'angularfire2';
import { HelperService } from '../providers/helperService';
// import { EventDetailPage } from '../pages/event-detail/event-detail';
import { PostModalPage } from '../pages/posts-modal/posts-modal';

@Component({
  selector: 'post-card',
  templateUrl: 'post-card.component.html'
})

export class PostCardComponent implements OnChanges {
  @Input('postData') postData: any;
  @Input('params') params: any;
  @ViewChild(PostDirective) post_container: PostDirective;

  currentAddIndex: number = -1;
  interval: any;
  uid: any;
  user: any = {};
  isLiked: boolean = false;
  likes: number;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public alert: AlertController,
    public app: App,
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    private _componentFactoryResolver: ComponentFactoryResolver,
    public helperService: HelperService,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public platform: Platform,
    private postService: PostService,
    public toastCtrl: ToastController,
  ) {
    this.helperService.getUid()
      .then((uid) => {
        this.uid = uid;
      })
  }

  ngOnChanges(changes: SimpleChanges) {
    let self: any = this;
    if (this.postData && this.postData.type) {
      this.afoDatabase.object('users/' + this.postData.createdBy)
        .take(1)
        .subscribe((user) => {
          this.user = user;
          this.postService.getComponent(this.postData)
            .then((Component: any) => {
              self.loadComponent(Component);
            })
        })
    }
  }

  loadComponent(postData: any) {
    let componentFactory = this._componentFactoryResolver.resolveComponentFactory(postData.component);
    let viewContainerRef = this.post_container.viewContainerRef;
    viewContainerRef.clear();
    let componentRef = viewContainerRef.createComponent(componentFactory);
    (<PostComponent>componentRef.instance).post = postData.post;
    (<PostComponent>componentRef.instance).params = this.params;
  }

  goToSessionDetail() {
    if (this.postData.type == 'CALENDAR') {
      // this.app.getRootNav().push(EventDetailPage, { event: this.postData.sessionId });
    }
  }

  showActionSheet() {
    if (!!!this.params.disableMore) {
      let buttons = [{
        text: 'Edit Post',
        icon: !this.platform.is('ios') ? 'create' : null,
        handler: () => {
          // this.navCtrl.push(PostAddPage, { id: this.postData.id });
        }
      },
      {
        text: 'Delete Post',
        icon: !this.platform.is('ios') ? 'trash' : null,
        role: 'destructive',
        handler: () => {
          let self = this;
          this.helperService.isAdmin()
            .then(() => {

              let alert = this.alert.create({
                title: 'Are you sure?',
                message: 'Deleting this cannot be undone',
                buttons: [
                  {
                    text: 'Cancel',
                    role: 'cancel'
                  },
                  {
                    text: 'Delete',
                    handler: () => {
                      self.af.database.object('likes/' + this.postData.id).remove()
                        .then(() => {
                          return self.af.database.object('posts/' + this.postData.id).remove()
                        })
                        .then(() => {
                          this.toastCtrl.create({
                            message: 'Post deleted successfully',
                            duration: 1500
                          }).present();
                        })
                        .catch(() => {
                          console.log('failed to delete the post');
                        })
                    }
                  }
                ]
              });
              alert.present();
            })
            .catch(() => {
            })
        }
      },
      {
        text: 'Cancel',
        role: 'cancel',
        icon: !this.platform.is('ios') ? 'close' : null
      }];

      if (this.postData.type === 'CALENDAR') {
        buttons.splice(0, 1);
      }
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Options',
        buttons: buttons
      });
      actionSheet.present();
    }
  }
  //opens the modal with the original post component
  openModal() {
    if (this.postData.type === 'YOUTUBE' || this.postData.type === 'POLL' || this.postData.type === 'IMAGE_GALLERY') {
      let modal = this.modalCtrl.create(PostModalPage, { post: this.postData });
      modal.present();
    }
  }

}
