import { Component, Input }  from '@angular/core';
import { PostComponent } from '../../post.component';
@Component({
  templateUrl: 'banner-with-description-post-component.html'
})
export class BannerWithDescriptionComponent implements PostComponent {
  @Input() params: any;
  @Input() post: any;
}
