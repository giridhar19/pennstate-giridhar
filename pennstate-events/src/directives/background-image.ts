import { Directive, Input, ElementRef, Renderer, OnChanges } from '@angular/core';
import { ImageCacheService } from '../providers/image-cache-services';

@Directive({
  selector: '[background-image]',
  providers: [ImageCacheService]
})

export class BackgroundImage implements OnChanges {

  constructor(
    public el: ElementRef,
    public imgCacheService: ImageCacheService,
    public renderer: Renderer) { }

  @Input('background-image') image: any;

  ngOnChanges() {
    if (this.image && this.image.includes('http')) {
      this.imgCacheService.isCached(this.image)
        .then((res: any) => {
          if (res) {
            this.imgCacheService.getCachedFileURL(this.image)
              .then((url: any) => {
                this.renderer.setElementStyle(this.el.nativeElement, 'background-image', 'url(' + url + ')');
                return;
              })
          }
          else {
            this.renderer.setElementStyle(this.el.nativeElement, 'background-image', 'url(' + this.image + ')');
            this.imgCacheService.cacheFile(this.image);
          }
        })
    }
    else {
      this.renderer.setElementStyle(this.el.nativeElement, 'background-image', 'url(' + this.image + ')');
    }
  }

}
